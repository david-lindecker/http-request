#include <ctype.h>
#include <curl/curl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct curl_slist* headers = NULL;
char* method;
char* url;
char* body;

char c;
void next() {
  c = getchar();
}

char* buffer;
size_t buffer_len;
size_t buffer_cap;

void buffer_init() {
  buffer = malloc(16);
  buffer_len = 0;
  buffer_cap = 16;
  buffer[0] = '\0';
}

void buffer_expand() {
  char* newstr = malloc(buffer_cap *= 2);
  memcpy(newstr, buffer, buffer_cap / 2);
  free(buffer);
  buffer = newstr;
}

void buffer_read() {
  if (buffer_len == buffer_cap - 1) buffer_expand();

  buffer[buffer_len++] = c;
  buffer[buffer_len] = '\0';
  
  next();
}

void parse_request() {
  next();

  for (;;) {
    buffer_init();

    while (c != ' ' && c != ':') buffer_read();

    if (
      strcmp(buffer, "DELETE") == 0 ||
      strcmp(buffer, "GET") == 0 ||
      strcmp(buffer, "PATCH") == 0 ||
      strcmp(buffer, "POST") == 0 ||
      strcmp(buffer, "PUT") == 0
    ) break;

    while (c != '\n') buffer_read();

    headers = curl_slist_append(headers, buffer);
    next();
  }

  method = buffer;

  buffer_init();
  while (isspace(c)) next();
  while (c != '\n') buffer_read();
  url = buffer;

  buffer_init();
  while (isspace(c)) next();
  while (c != EOF) buffer_read();
  body = buffer;

  // rtrim body
  int i = buffer_len - 1;
  while (i >= 0 && isspace(body[i])) body[i--] = '\0';
}

size_t header_callback(char* buffer, size_t size, size_t nitems, void* userdata) {
  if (nitems == 2) return nitems;
  for (size_t i = 0; i < nitems; ++i) fputc(buffer[i], stderr);
  return nitems;
}

void perform_request() {
  curl_global_init(CURL_GLOBAL_ALL);
  CURL* curl = curl_easy_init();

  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method);
  curl_easy_setopt(curl, CURLOPT_URL, url);
  if (strcmp(body, "") != 0) curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body);
  curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, &header_callback);

  CURLcode result = curl_easy_perform(curl);

  curl_easy_cleanup(curl);
  curl_global_cleanup();
}

int main() {
  parse_request();
  perform_request();
  putchar('\n');
  
  return EXIT_SUCCESS;
}
