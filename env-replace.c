#include <stdio.h>
#include <stdlib.h>

int main() {
  int state = 0;
  char c;

  char name[1024];
  int i = 0;

  while ((c = getchar()) != EOF) {
    switch (state) {
      case 0:
        if (c == '{') {
          state = 1;
        } else {
          putchar(c);
        }
        break;

      case 1:
        if (c == '{') {
          state = 2;
        } else {
          putchar('{');
          putchar(c);
          state = 0;
        }
        break;

      case 2:
        if (c == '}') {
          state = 3;
        } else {
          name[i++] = c;
        }
        break;

      case 3:
        if (c == '}') {
          name[i] = '\0';
          printf("%s", getenv(name));
          i = 0;
          state = 0;
        } else {
          name[i++] = '}';
          name[i++] = c;
          state = 2;
        }
        break;
    }
  }

  return EXIT_SUCCESS;
}
