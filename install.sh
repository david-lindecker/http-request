#!/bin/sh

mkdir -p $HOME/bin
echo "#!/bin/sh\n\ncat \$1 | $PWD/env-replace | $PWD/perform-request" >$HOME/bin/http-request
chmod +x $HOME/bin/http-request
