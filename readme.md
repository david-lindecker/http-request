# http-request

A simple utility for defining and performing HTTP requests. Requires libcurl.

## Installation

To build the executables and install an orchestration script in your home bin directory, run:

```
$ make install
```

Your home bin directory will need to be in your `PATH`.
Try it out by running `http-request ./sample`
