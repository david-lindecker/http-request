CC=gcc
CFLAGS=-std=c99

all: perform-request env-replace

perform-request: perform-request.c
	${CC} -o perform-request ${CFLAGS} perform-request.c -lcurl

env-replace: env-replace.c
	${CC} -o env-replace ${CFLAGS} env-replace.c

install: perform-request env-replace
	./install.sh

clean:
	rm -f perform-request env-replace
